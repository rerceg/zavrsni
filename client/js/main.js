const address = "http://192.168.1.4:3000/";

var joystickConf = {
      zone: document.querySelector('.zone'),
      mode: 'static',
      position: {
        left: '50%',
        top: '50%'
      },
      color: 'blue',
      size: 150
    };

var y_sredina = 0;
var joystick = nipplejs.create(joystickConf);

joystick.on('move end', function(evt, data) {
  move(data);
}).on('start', function(evt, data){
  if(y_sredina === 0) y_sredina = data.position.y;
});

function inPlaceRotate(id){
  (async () => {
      const rawResponse = await fetch(address + id, {
        method: 'POST'
      });

      const content = await rawResponse.status;

    })();
}

function move(obj) {
  var {position, angle} = obj;
  var y = -position.y + y_sredina;
  var joystickData =  {"position" : {"x" : position.x, "y": y}, angle};

  (async () => {
    const rawResponse = await fetch(address, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(joystickData)
    });

    const content = await rawResponse.status;

  })();
}