const express = require('express');
const bodyParser = require('body-parser');
const i2c = require('i2c-bus');

const app = express();
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const route = '/'
const address = 0xA;
const stop = Buffer.from([0,0,0,0,0,0,0,0]);
const reset = Buffer.from([10]);

var send = 1;

app.post(route, (req, res) => {
  const body = req.body;
  if(send){
	  let lijevi = 0;
	  let desni = 0;
		if(typeof(body.angle) !== 'undefined') {
			var angle = 0; 
			
			if(body.angle.radian <= Math.PI) angle = body.angle.radian - Math.PI/2;
			else angle = body.angle.radian - 3*Math.PI/2;  
			
			desni = Math.floor(1000*(body.position.y/3 + angle*8.5)/(6.5));
			lijevi = Math.floor(1000*(body.position.y/3 - angle*8.5)/(6.5));
		}

	  lijevi_znamenke = znamenke(lijevi);
	  desni_znamenke = znamenke(desni);

		const wbuf = Buffer.from(desni_znamenke.concat(lijevi_znamenke));
		send = 0;
	 	sendToMega(wbuf)
	} else {
		if (typeof(body.angle) === 'undefined') setTimeout(sendToMega, 100, stop);
	}
  res.sendStatus(200);
});

app.post(route + ":id", (req, res) => {
	if(send){
		const id = req.params.id
		var lijevi_znamenke;
		var desni_znamenke;
		if(id == 2){
			lijevi_znamenke = [1,8,0,0];
			desni_znamenke = [-1,8,0,0];
		} else if(id == 1){
			lijevi_znamenke = [-1,8,0,0];
			desni_znamenke = [1,8,0,0];
		} else if(id == 0){
			lijevi_znamenke = [0,0,0,0];
			desni_znamenke = [0,0,0,0];
		}
		const wbuf = Buffer.from(desni_znamenke.concat(lijevi_znamenke));
	 	send = 0;
		sendToMega(wbuf);
	}
	res.sendStatus(200);
});

app.listen(3000, () => console.log('Listening on port 3000'));

function sendToMega(wbuf) {
	i2c.openPromisified(1).
	then(i2c1 => i2c1.i2cWrite(address, wbuf.length, wbuf).
	then(_ => {i2c1.close(); send = 1;})).catch((error) => {
		send = 0
		setTimeout(sendToMega, 100, reset);
	});
}

function znamenke(broj) {
	let array = [1,1,1,1];
	if(broj < 0) {
		if(broj > -10) array[3] = -1;
		else if(broj > -100) array[2] = -1;
		else if(broj > -1000) array[1] = -1;
		else array[0] = -1;
		broj *= -1;
	}
	for (var i = 3; i >= 0; i--) {
		array[i] *= broj%10;
		broj = Math.floor(broj/10);
	}
	return array;
}
