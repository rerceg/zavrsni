#ifndef MOTOR_H_
#define MOTOR_H_

#include <avr/io.h>

void motor_init() {
	DDRB |= (1 << DDB6) | (1 << DDB5);
	DDRH |= (1 << DDE3) | (1 << DDE4);

	TCCR1B |= (1 << CS10);
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
	
	TCCR4B |= (1 << CS40);
	TCCR4A |= (1 << COM4A1) | (1 << COM4B1);

	TCCR1A |= (1 << WGM11) | (1 << WGM10);
	TCCR4A |= (1 << WGM41) | (1 << WGM40);
}

#endif