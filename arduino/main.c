#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h> //I2c

#include "serial.h"
#include "encoder.h"
#include "motor.h"
#include "counter.h"

volatile uint64_t ms = 0;

volatile int32_t P, I, D;

volatile int64_t err1 = 0;
volatile int64_t err_old1 = 0;
volatile int64_t err_acc1 = 0;
volatile int64_t err_diff1 = 0;

volatile int64_t err2 = 0;
volatile int64_t err_old2 = 0;
volatile int64_t err_acc2 = 0;
volatile int64_t err_diff2 = 0;

volatile int64_t gain1 = 0;
volatile int16_t speed1 = 0;

volatile int64_t gain2 = 0;
volatile int16_t speed2 = 0;

volatile int64_t max = 1023;

volatile int16_t tps1 = 0;
volatile int16_t tps_m1 = 0;

volatile int16_t tps2 = 0;
volatile int16_t tps_m2 = 0;

volatile int16_t tps1_temp = 0;

void reset_errors() {
	err1 = 0;
	err_old1 = 0;
	err_acc1 = 0;
	err_diff1 = 0;
	err2 = 0;
	err_old2 = 0;
	err_acc2 = 0;
	err_diff2 = 0;
}

ISR(TIMER0_COMPA_vect) {
	++ms;
	if(ms % 5 == 0) {
		tps_m1 = (ticks1 - ticks_old1)*1000 / 5;
		
		err1 = tps1 - tps_m1;
		err_acc1 += err1;
		err_diff1 = err1 - err_old1;
		gain1 = P * err1 + I * err_acc1 + D * err_diff1;
		gain1 /= 50;
		
		if(gain1 > 0) {
			speed1 = gain1 > max ? max : gain1;
			OCR1A = speed1;
			OCR1B = 0;
		}
		else {
			gain1 *= -1;
			speed1 = gain1 > max ? max : gain1;
			OCR1B = speed1;
			OCR1A = 0;
		}
		
		tps_m2 = (ticks2 - ticks_old2)*1000 / 5;
		
		err2 = tps2 - tps_m2;
		err_acc2 += err2;
		err_diff2 = err2 - err_old2;
		gain2 = P * err2 + I * err_acc2 + D * err_diff2;
		gain2 /= 50;
		
		if(gain2 > 0) {
			speed2 = gain2 > max ? max : gain2;
			OCR4A = speed2;
			OCR4B = 0;
		}
		else {
			gain2 *= -1;
			speed2 = gain2 > max ? max : gain2;
			OCR4B = speed2;
			OCR4A = 0;
		}
		
		err_old1 = err1;
		ticks_old1 = ticks1;
		err_old2 = err2;
		ticks_old2 = ticks2;
	}
}

uint8_t address = 10;

void i2c_init() {
	// postavi adresu
	// AR - address register
	// 7 najzna?ajnijih bitova na adresu
	TWAR = address << 1;

	// TWIE - enejblaj interrupt
	// TWEA - omogu?i uparivanje adresa
	// TWEN - enejblaj I2C
	TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWEN) | (1 << TWINT);
}

volatile uint16_t r;
int cetveroznamenkasti = 0;
int flag = 0;
int flag2 = 0;
int negativan = 0; 

ISR(TWI_vect) {
	// slu?ajevi:
	// master �alje podatke
	// master zahtjeva podatke
	// gre�ka
	
	// svi su slu?ajevi unutar TW_STATUS
	switch(TW_STATUS) {
		// master �alje podatke
		case TW_SR_DATA_ACK:
		flag++;
		r = TWDR;
		if(r == 10){
			flag = 0;
			flag2 = 0;
			negativan = 0;
			cetveroznamenkasti = 0;
			TWCR = 0;
			TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWEN) | (1 << TWINT);
			break;
		}
		if (r > 127) {
			r = (256 - r)*-1;
			negativan = 1;
		}
		if(negativan && cetveroznamenkasti != 0) cetveroznamenkasti = cetveroznamenkasti*10 -r;
		else cetveroznamenkasti = cetveroznamenkasti*10 + r;
		if(flag == 4){
			negativan = 0;
			flag = 0;
			if (flag2) {
				flag2 = 0;
				tps2 = cetveroznamenkasti;
				tps1 = tps1_temp;
				tps1_temp = 0;
				reset_errors();
			}
			else {
				flag2 = 1;
				tps1_temp = cetveroznamenkasti;
			}
			cetveroznamenkasti = 0;
		}
		// postavi I2C na inicijalno stanje
		TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWEN) | (1 << TWINT);
		break;
		
		case TW_BUS_ERROR:
		TWCR = 0;
		TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWEN) | (1 << TWINT);
		break;
		
		default:
		TWCR = (1 << TWIE) | (1 << TWEA) | (1 << TWEN) | (1 << TWINT);
		break;
		
	}
}

int main() {
	encoder_init();
	motor_init();
	counter_init();
	i2c_init();
	
	DDRD |= (1 << DDD0) | (1 << DDD1);
	PORTD |= (1 << PORTD0) | (1 << PORTD1);
	
	P = 30; //40
	I = 6; //11
	D = 10; //13
	
	tps1 = 0; //desni tocak
	tps2 = 0;

	sei();
	
	while(1){
	}
	
	return 0;
}