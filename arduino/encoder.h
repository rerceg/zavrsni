#ifndef ENCODER_H_
#define ENCODER_H_

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t A1, B1, A2, B2;
volatile int32_t ticks1 = 0;
volatile int32_t ticks2 = 0;
volatile int32_t ticks_old1 = 0;
volatile int32_t ticks_old2 = 0;

ISR(INT4_vect) {
	A1 != B1 ? --ticks1 : ++ticks1;
	A1 = PINE & (1 << PINE4) ? 1 : 0;
}

ISR(INT5_vect) {
	B1 = PINE & (1 << PINE5) ? 1 : 0;
	A1 != B1 ? --ticks1 : ++ticks1;
}

ISR(INT2_vect) {
	A2 != B2 ? ++ticks2 : --ticks2;
	A2 = PIND & (1 << PIND2) ? 1 : 0;
}

ISR(INT3_vect) {
	B2 = PIND & (1 << PIND3) ? 1 : 0;
	A2 != B2 ? ++ticks2 : --ticks2;
}

void encoder_init() {
	DDRE &= ~(1 << DDD4);
	DDRE &= ~(1 << DDD5);
	DDRD &= ~(1 << DDD2);
	DDRD &= ~(1 << DDD3);

	EICRA |= (1 << ISC20) | (1 << ISC30);
	EICRB |= (1 << ISC40) | (1 << ISC50);
	EIMSK |= (1 << INT4) | (1 << INT5) | (1 << INT2) | (1 << INT3);
	sei();
}

#endif