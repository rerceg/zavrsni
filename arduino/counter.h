#ifndef COUNTER_H_
#define COUNTER_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void counter_init() {
	TCCR0B |= (1 << CS01) | (1 << CS00);
	TIMSK0 |= (1 << OCIE0A);
	
	TCCR0A |= (1 << WGM01);
	TCCR0A |= (1 << COM0A1);
	
	OCR0A = 250;
	sei();
}

#endif